<?php
/*
 * Plugin Name: WooCommerce CV Service Payment Gateway
 * Plugin URI:https://gitlab.com/vision-inc/cv-service-gateway/-/boards
 * Description: Take credit card payments on your store.
 * Author: Hai Phan Nguyen
 * Author URI: https://github.com/pnghai
 * Version: 1.0.0
 */
add_filter('woocommerce_payment_gateways', 'cv_service_add_gateway_class');
function cv_service_add_gateway_class($gateways)
{
    $gateways[] = 'WC_CvService_Gateway'; // your class name is here
    return $gateways;
}

//if( ! function_exists( 'debug_wp_remote_post_and_get_request' ) ) :
//    function debug_wp_remote_post_and_get_request( $response, $context, $class, $r, $url ) {
//        error_log( '------------------------------' );
//        error_log( $url );
//        error_log( json_encode( $response ) );
//        error_log( $class );
//        error_log( $context );
//        error_log( json_encode( $r ) );
//    }
//    add_action( 'http_api_debug', 'debug_wp_remote_post_and_get_request', 10, 5 );
//endif;
/*
 * The class itself, please note that it is inside plugins_loaded action hook
 */
add_action('plugins_loaded', 'cv_service_init_gateway_class');
function cv_service_init_gateway_class()
{

    class WC_CvService_Gateway extends WC_Payment_Gateway
    {
        /**
         * Class constructor, more about it in Step 3
         */
        public function __construct()
        {

            $this->id = 'cv_service'; // payment gateway plugin ID
            $this->icon = ''; // URL of the icon that will be displayed on checkout page near your gateway name
            $this->has_fields = false; // in case you need a custom credit card form
            $this->method_title = 'CvService Gateway';
            $this->method_description = 'Description of CvService payment gateway'; // will be displayed on the options page
            // gateways can support subscriptions, refunds, saved payment methods,
            // but in this tutorial we begin with simple payments
            $this->supports = array(
                'products'
            );

            // Method with all the options fields
            $this->init_form_fields();

            // Load the settings.
            $this->init_settings();
            $this->enabled = $this->get_option('enabled');
            $this->successful_note = $this->get_option('successful_note');
            $this->failed_note = $this->get_option('failed_note');
            $this->cancelled_note = $this->get_option('cancelled_note');
            $this->testmode = 'yes' === $this->get_option('testmode');
            $this->description = $this->get_option('description');
            if ($this->testmode) {
                $this->description .= __('!!!SANDBOX MODE!!!', 'cvs_gateway');
            }
            $this->order_button_text = $this->get_option('order_button_text');
            $this->private_key = $this->testmode ? $this->get_option('test_private_key') : $this->get_option('private_key');
            $this->publishable_key = $this->testmode ? $this->get_option('test_publishable_key') : $this->get_option('publishable_key');
            $this->whitelist_ips = $this->testmode ? [] : explode(',', str_replace(' ', '', $this->get_option('whitelist_ips')));

            // This action hook saves the settings
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));

            // below is the hook you need for form forward purpose
            add_action('woocommerce_receipt_' . $this->id, array(
                $this,
                'pay_for_order'
            ));

            // We need custom JavaScript to obtain a token
//            add_action( 'wp_enqueue_scripts', array( $this, 'payment_scripts' ) );

            // You can also register a webhook here
//             add_action( 'woocommerce_api_cvs_payment_cb', array( $this, 'payment' ) );
            add_action('woocommerce_api_cvs_gateway_cb', array($this, 'kickback'));
//            add_action('woocommerce_api_cvs_success_cb', array($this, 'success'));
//            add_action('woocommerce_api_cvs_failed_cb', array($this, 'failed'));
            add_action('woocommerce_api_cvs_cancelled_cb', array($this, 'cancelled'));

        }

        /**
         * Plugin options, we deal with it in Step 3 too
         */
        public function init_form_fields()
        {

            $this->form_fields = array(
                'enabled' => array(
                    'title' => __('Enable/Disable', 'cvs_gateway'),
                    'label' => __('Enable CvService Gateway', 'cvs_gateway'),
                    'type' => 'checkbox',
                    'description' => '',
                    'default' => 'no'
                ),
                'description' => array(
                    'title' => __('Description', 'cvs_gateway'),
                    'label' => __('Gateway Description', 'cvs_gateway'),
                    'type' => 'textarea',
                    'default' => '',
                ),
                'order_button_text' => array(
                    'title' => __('Order Button Text', 'cvs_gateway'),
                    'label' => __('Order Button Text', 'cvs_gateway'),
                    'type' => 'text',
                    'default' => 'クレジットカード決済へ進む',
                ),
                'whitelist_ips' => array(
                    'title' => __('White list IP(s)', 'cvs_gateway'),
                    'label' => __('White list IP(s)', 'cvs_gateway'),
                    'description' => __('Enter list of IP, separated by comma (,)', 'cvs_gateway'),
                    'type' => 'textarea',
                    'default' => '',
                ),
                'successful_note' => array(
                    'title' => __('Successful note', 'cvs_gateway'),
                    'type' => 'text',
                    'default' => __('Order was paid', 'cvs_gateway'),
                ),
                'failed_note' => array(
                    'title' => __('Failed note', 'cvs_gateway'),
                    'type' => 'text',
                    'default' => __('Order was failed', 'cvs_gateway'),
                ),
                'cancelled_note' => array(
                    'title' => __('Cancelled note', 'cvs_gateway'),
                    'type' => 'text',
                    'default' => __('Order was Cancelled', 'cvs_gateway'),
                ),
                'testmode' => array(
                    'title' => __('Test mode', 'cvs_gateway'),
                    'label' => __('Enable Test Mode', 'cvs_gateway'),
                    'type' => 'checkbox',
                    'description' => __('Place the payment gateway in test mode using test API keys.', 'cvs_gateway'),
                    'default' => 'yes',
                    'desc_tip' => true,
                ),
                'test_publishable_key' => array(
                    'title' => __('Test Publishable Key', 'cvs_gateway'),
                    'type' => 'text'
                ),
                'test_private_key' => array(
                    'title' => __('Test Private Key', 'cvs_gateway'),
                    'type' => 'password',
                ),
                'publishable_key' => array(
                    'title' => __('Live Publishable Key', 'cvs_gateway'),
                    'type' => 'text'
                ),
                'private_key' => array(
                    'title' => __('Live Private Key', 'cvs_gateway'),
                    'type' => 'password'
                )
            );


        }


        /*
         * Custom CSS and JS, in most cases required only when you decided to go with a custom credit card form
         */
        public function payment_scripts()
        {


        }

//        /*
//          * Fields validation, more in Step 5
//         */
//        public function validate_fields() {
////            if( empty( $_POST[ 'billing_first_name' ]) ) {
////                wc_add_notice(  'First name is required!', 'error' );
////                return false;
////            }
////            return true;
////
//
//        }
        public const CSV_ERROR_CODES = array(
            1001 => '決済要求で POST データが空',
            1002 => 'clientip エラー',
            1005 => 'price エラー',
            1006 => 'sendid エラー',
            1007 => 'order_num エラー',
            1008 => 'email エラー',
            1009 => 'clientip が存在しない',
            1010 => 'tel エラー',
            1013 => '番組が停止か解約済み',
            1014 => 'リファラーが不正',
            1015 => 'card_num エラー',
            1016 => 'expire_year, expire_month エラー',
            1017 => 'last_name, first_name エラー',
            1018 => 'name_type エラー',
            1019 => 'cvv エラー',
            1020 => 'birth_month エラー',
            1021 => 'birth_day エラー',
            1022 => 'SMS の電話番号が不正または SMS エラー',
            1023 => 'cycle_code エラー',
            1024 => 'cycle_price エラー',
            1025 => 'product_name エラー',
            1026 => 'cycle_type エラー',
            1027 => 'sendsid エラー',
            1028 => 'kind エラー',
            1029 => 'free_param エラー',
            2001 => 'session データエラー',
            2002 => 'token エラー',
            2003 => 'post データエラー',
            2005 => '決済可能金額エラー',
            2006 => 'ゲートウェイ通信エラー',
            2007 => 'ゲートウェイパラメータエラー',
            2009 => '重複決済エラー',
            2011 => '海外 IP アドレス拒否',
            2012 => '海外クレジットカード拒否',
            2013 => '利用不可クレジットカード拒否',
            2014 => 'ブラックリストユーザー拒否',
            2015 => '拒否設定による拒否',
            2016 => 'ゲートウェイが稼働状態でない',
            2017 => 'クレジットカード情報が取得できない',
            2018 => '番組設定の上限額が不正',
            2019 => '為替手数料が 25%を超えている',
            2020 => '該当クレジットブランドに対して、ゲートウェイが設定されていない',
            2022 => '24 時間連続決済失敗回数に達した',
            2024 => 'リカーリング退会時に、cycle_code から取得したパラメータが不正',
            7000 => '画面なしクイック決済成功',
            7001 => 'クイック決済用クレジットデータが存在しない',
            7002 => 'クイック決済限度回数超越',
            7003 => '画面なしクイック決済が許可されていない IP アドレスからのアクセス',
        );

        /*
         * We're processing the payments here, everything about it is in Step 5
         */
        public function process_payment($order_id)
        {
            // we need it to get any order details
            $order = wc_get_order($order_id);

            return array(
                'result' => 'success',
                'redirect' => $order->get_checkout_payment_url(true)
            );

//            if ( $order->get_total() > 0 ) {
//                $order->update_status( apply_filters( 'woocommerce_csv_process_payment_order_status', 'on-hold', $order ), _x( 'Awaiting cvs payment', 'Cvs payment method', 'cvs_gateway' ) );
//            } else {
//                $order->payment_complete();
//            }
//
//            WC()->cart->empty_cart();
//            /*
//              * Array with parameters for API interaction
//             */
//            $expiry = explode('/', str_replace(' ', '', sanitize_text_field($_POST[  esc_attr( $this->id . '-card-expiry' ) ])));
//            $args = array(
//                'clientip'=>$this->private_key,
//                'sendid'=>(string)($order->get_user_id()),
//                'price'=>(string)($order->get_total()),
//                'order_num'=> (string)$order_id,
//                'email'=>$order->get_billing_email(),
//                'tel'=>$order->get_billing_phone(),
//                'card_num'=>str_replace(' ', '',sanitize_text_field($_POST[  esc_attr( $this->id . '-card-number' ) ])),
//                'expire_year'=>(string)$expiry[1],
//                'expire_month'=>(string)$expiry[0],
//                'cvv'=>sanitize_text_field($_POST[  esc_attr( $this->id . '-card-cvc' ) ]),
//                'last_name'=>$order->get_billing_last_name(),
//                'first_name'=>$order->get_billing_first_name(),
//            );
////                'sendsid',
////                'kind',
////                'free_param'
//
//            /*
//             * Your API interaction could be built with wp_remote_post()
//              */
//            $response = wp_remote_post( 'https://secure.cv-service.site/quick_back', array(
//                'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
//                'body' => $args
//            ));
//
//            if( !is_wp_error( $response ) ) {
//                $body = $response['body'];
//                $result = explode(':',$body);
//                if ( (int)$result[0] === 7000 ) {
//                    $this->order_success($order_id);
//                    // Redirect to the thank you page
//                    return array(
//                        'result' => 'success',
//                        'redirect' => $this->get_return_url( $order )
//                    );
//                }
//                $error_msg = self::CSV_ERROR_CODES[(int)$result[0]];
//                $logger = wc_get_logger();
//                if ($logger){
//                    $logger->error( $order_id . $error_msg, array( 'order' => $order, 'source' => 'csv', 'raw_error'=> $result ) );
//                }
//                $this->order_failed($order_id);
//                wc_add_notice( $error_msg, 'error' );
////                WC()->session->set( 'reload_checkout ', 'true' );
//                return;
////                    array(
////                    'result'   => 'failure',
////                    'redirect' => $order->get_checkout_payment_url()
////                );
//            }
//            wc_add_notice(  __( 'Please try again.', 'cvs_gateway' ), 'error' );
////            WC()->session->set( 'reload_checkout ', 'true' );
//            return;
        }

// here, prepare your form and submit it to the required URL
        public function pay_for_order($order_id)
        {
            $order = new WC_Order($order_id);
            echo '<p>' . __('Redirecting to payment provider...', 'cvs_gateway') . '</p>';
            // add a note to show order has been placed and the user redirected
            $order->add_order_note(__('Order placed and user redirected.', 'cvs_gateway'));
            // update the status of the order should need be
            $order->update_status('on-hold', __('Awaiting payment.', 'cvs_gateway'));
            // remember to empty the cart of the user
            WC()->cart->empty_cart();

            // perform a click action on the submit button of the form you are going to return
            wc_enqueue_js('jQuery( "#submit-form" ).click();');
            $endpoint = 'https://secure.cv-service.site/input';
            // return your form with the needed parameters
            echo "<form action='$endpoint' method='post' target='_top' style='display: none;'>
        <input type='hidden' name='clientip' value='{$this->private_key}'>
        <input type='hidden' name='sendid' value='{$order->get_user_id()}'>
        <input type='hidden' name='price' value='{$order->get_total()}'>
        <input type='hidden' name='order_num' value='$order_id'>
        <input type='hidden' name='email' value='{$order->get_billing_email()}'>
        <input type='hidden' name='tel' value='{$order->get_billing_phone()}'>
        <div class='btn-submit-payment'><button type='submit' id='submit-form'></button></div>
        </form>";
        }

        protected function log($type, $order_id){
            $logger = wc_get_logger();
            if ($logger){
                $logger->notice( "$type - $order_id", array( 'post_data' => $_POST ));
            }
        }

        public function kickback():void {
            $order_id = wc_sanitize_order_id((int)$_POST['order_num']);
            $ip = $_SERVER['REMOTE_ADDR'];
            if (!in_array($ip, $this->whitelist_ips)){
                $this->log('error', 'hacked attempt from'.$ip);
                return;
            }
            if ($_POST['result'] === 'OK'){
                $this->order_success($order_id);
                $this->log('success', $order_id);
            } else {
                $this->order_failed($order_id);
                $this->log('failed', $order_id);
            }
        }

        public function order_success($order_id): void
        {
            $order = wc_get_order($order_id);
            $order->payment_complete();
            $order->add_order_note($this->successful_note, true);
        }

        public function order_failed($order_id): void
        {
            $order = wc_get_order($order_id);
            $order->update_status('failed', $this->failed_note);
        }

        public function cancelled(): void
        {
            $order_id = wc_sanitize_order_id((int)$_POST['order_num']);
            $order = wc_get_order($order_id);
            $order->update_status('cancelled', $this->cancelled_note);
            $this->log('cancelled', $order_id);
        }
    }
}